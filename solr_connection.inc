<?php

class SarniaSearchApiSolrConnection extends SearchApiSolrConnection {

  /**
   * Implements SearchApiSolrConnectionInterface::getFields().
   */
  public function getFields($num_terms = 0) {
    $fields = array();
    foreach ($this->getLuke($num_terms)->fields as $name => $info) {
      $fields[$name] = new SarniaSearchApiSolrField($info);
    }
    return $fields;
  }

}
